package models.enums;

import com.avaje.ebean.annotation.EnumValue;

public enum AccountRole {

    @EnumValue("ADMIN")
    ADMIN,

    @EnumValue("USER")
    USER

}
