package services;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.PagedList;
import models.User;
import org.mindrot.jbcrypt.BCrypt;

import java.util.List;

public class UserDAO {

    public static String hashPassword(String clearString) {
        if (clearString == null) {
            return null;
        }
        return BCrypt.hashpw(clearString, BCrypt.gensalt());
    }

    public static User findByNameAndEmail(String name, String email) {

        User user = Ebean.find(User.class)
                .where()
                .or(com.avaje.ebean.Expr.eq("name", name), com.avaje.ebean.Expr.eq("email", email))
                .findUnique();

        if (user != null) {
            return user;
        }

        return null;
    }

    public static User findUserByNameAndPassword(String name, String password) {

        User user = Ebean.find(User.class)
                .where()
                .ieq("name", name)
                .findUnique();

        if (user != null) {
            if (BCrypt.checkpw(password, user.getPassword())) {
                return user;
            }
        }

        return null;
    }

    public static User findUserByEmailAndPassword(String email, String password) {

        User user = Ebean.find(User.class)
                .where()
                .ieq("email", email)
                .findUnique();

        if (user != null) {
            if (BCrypt.checkpw(password, user.getPassword())) {
                return user;
            }
        }

        return null;
    }

    public static User findById(long userId) {
        return Ebean.find(User.class)
                .where()
                .idEq(userId)
                .findUnique();
    }

    public static User findByName(String name) {
        return Ebean.find(User.class)
                .where()
                .ieq("name", name)
                .findUnique();
    }

    public static User findByEmail(String email) {
        return Ebean.find(User.class)
                .where()
                .ieq("email", email)
                .findUnique();
    }

    public static List<User> getAll() {
        return Ebean.find(User.class)
                .where()
                .orderBy("id asc")
                .findList();
    }

    public static PagedList<User> paginatedUserList(int page, int pageSize) {
        PagedList<User> pagedList = Ebean.find(User.class)
                .where()
                .orderBy("id asc")
                .setFirstRow(page)
                .findPagedList(page, pageSize);

        return pagedList;
    }

}
