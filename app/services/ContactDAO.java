package services;

import com.avaje.ebean.*;
import models.Contact;
import models.User;

import java.util.List;

/**
 * Created by User on 2016-07-10.
 */
public class ContactDAO {

    public static Contact findById(long contactId) {
        return Ebean.find(Contact.class)
                .where()
                .idEq(contactId)
                .findUnique();
    }

    public static Contact findByIdAndUser(long contactId, long userId) {
        return Ebean.find(Contact.class)
                .where()
                .idEq(contactId)
                .eq("user_id", userId)
                .findUnique();
    }

    public static List<Contact> getAllForUser(User user) {
        return Ebean.find(Contact.class)
                .where()
                .eq("user_id", user.getId())
                .orderBy("id asc")
                .findList();
    }

    public static PagedList<Contact> paginatedContactList(User user, int page, int pageSize) {
        PagedList<Contact> pagedList = Ebean.find(Contact.class)
                .where()
                .eq("user_id", user.getId())
                .orderBy("id asc")
                .setFirstRow(page)
                .findPagedList(page, pageSize);

        return pagedList;
    }

}
