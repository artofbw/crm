package controllers;

import com.google.inject.Inject;
import models.Contact;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.ContactDAO;

@Security.Authenticated(SecurityController.class)
public class ContactController extends Controller {

    @Inject
    private FormFactory formFactory;


    public Result blank() {

        Form<Contact> bookForm = formFactory.form(Contact.class).bindFromRequest();
        return ok(views.html.contact.add.render(bookForm));
    }

    public Result add() {

        User user = SecurityController.getUser();

        Form<Contact> bookForm = formFactory.form(Contact.class).bindFromRequest();
        bookForm.get().setUser(user);
        bookForm.get().save();
        return ok(views.html.contact.add.render(bookForm));
    }

    public Result list(int page) {

        User user = SecurityController.getUser();
        return ok(views.html.contact.list.render(ContactDAO.paginatedContactList(user, page, 15)));

    }

    public Result edit(long id) {
        User user = SecurityController.getUser();
        Contact contact = ContactDAO.findByIdAndUser(id, user.getId());

        Form<Contact> filledContact = formFactory.form(Contact.class).fill(contact);
        return ok(views.html.contact.edit.render(id, filledContact));
    }

    public Result update(long id) {

        User user = SecurityController.getUser();

        Form<Contact> contactForm = formFactory.form(Contact.class).bindFromRequest();
        Contact contact = ContactDAO.findByIdAndUser(id, user.getId());

        contact.setFirstName(contactForm.get().getFirstName());
        contact.setLastName(contactForm.get().getLastName());
        contact.setCompany(contactForm.get().getCompany());
        contact.setEmail(contactForm.get().getEmail());
        contact.setTel(contactForm.get().getTel());

        contact.update();

        flash("contactUpdated", "Contact has been successfully updated");

        return redirect(routes.ContactController.list(0));
    }

    public Result delete(long id) {
        User user = SecurityController.getUser();
        Contact contact = ContactDAO.findByIdAndUser(id, user.getId());
        contact.delete();
        System.out.println(user.getId());
        flash("contactRemoved", "Contact has been removed");
        return redirect(routes.ContactController.list(0));
    }

}
