package controllers;

import models.User;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.UserDAO;

public class SecurityController extends Security.Authenticator {
    @Override
    public String getUsername(Http.Context ctx) {
        if(ctx.session().get("name") == null) {
            return null;
        }

        User user = UserDAO.findById(Long.valueOf(ctx.session().get("userId")));

        if(user != null) {
            ctx.args.put("user", user);
            return user.getName();
        }
        return null;
    }

    public static User getUser() {
        return (User) Http.Context.current().args.get("user");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return forbidden(views.html.error.render("You must be logged in to view this area."));
    }

}
