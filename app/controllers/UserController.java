package controllers;

import com.google.inject.Inject;
import models.User;
import models.enums.AccountRole;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.UserDAO;

import java.io.File;

@Security.Authenticated(SecurityController.class)
public class UserController extends Controller {

    @Inject
    private FormFactory formFactory;

    public Result list(int page) {
        if(session().get("userRole").equalsIgnoreCase(AccountRole.ADMIN.toString())) {
            return ok(views.html.admin.user.list.render(UserDAO.paginatedUserList(page, 15)));
        } else {
            return forbidden(views.html.error.render("No administrator privileges"));
        }
    }

    public Result editProfile() {
        User user = SecurityController.getUser();
        Form<User> profile = formFactory.form(User.class).fill(user);
        return ok(views.html.profile.edit.render(profile));
    }

    public Result updateProfile() {

        Form<User> filledForm = formFactory.form((User.class)).bindFromRequest();
        User user = SecurityController.getUser();

        Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart<File> filePart = body.getFile("picture");

        if(filledForm.get() != null) {

            String name = filledForm.get().getName();
            String email = filledForm.get().getEmail();
            String password = filledForm.get().getPassword();
            File file = filePart.getFile();
            String fileName = filePart.getFilename();

            if(name != null && !name.isEmpty()) {
                if(UserDAO.findByName(name) != null && !name.equalsIgnoreCase(String.valueOf(user.getName()))) {
                    return redirect(routes.UserController.editProfile());
                }
                user.setName(name);
            }
            if(password != null && !password.isEmpty()) {
                user.setPassword(UserDAO.hashPassword(password));
            }
            if(email != null && !email.isEmpty()) {
                if(UserDAO.findByEmail(email) != null && !email.equalsIgnoreCase(String.valueOf(user.getEmail()))) {
                    flash("emailExists", "This email already existis");
                    return redirect(routes.UserController.editProfile());
                }
                user.setEmail(email);
            }
            if(filePart != null && !fileName.isEmpty()) {
                file.renameTo(new File("public/images/picture", fileName));
                user.setPicture(fileName);
            }

            user.update();
            session().put("name", name);

        }

        return redirect(routes.UserController.editProfile());
    }
}
