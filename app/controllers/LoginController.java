package controllers;

import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.UserDAO;

import javax.inject.Inject;

public class LoginController extends Controller {

    @Inject
    private FormFactory formFactory;

    public Result blank() {
        if(session().isEmpty()) {
            return ok(views.html.login.form.render());
        } else {
            return redirect(routes.Application.index());
        }

    }

    public Result login() {

        Form<User> loginForm = formFactory.form(User.class).bindFromRequest();

        String login = loginForm.data().get("login");
        String password = loginForm.data().get("password");

        User userByName = UserDAO.findUserByNameAndPassword(login, password);
        User userByEmail = UserDAO.findUserByEmailAndPassword(login, password);

        if(userByName != null) {
            session("userId", String.valueOf(userByName.getId()));
            session("name", String.valueOf(userByName.getName()));
            session("userRole", String.valueOf(userByName.getAccountRole()));
            return redirect(routes.Application.index());
        } else if(userByEmail != null) {
            session("userId", String.valueOf(userByEmail.getId()));
            session("name", String.valueOf(userByEmail.getName()));
            session("userRole", String.valueOf(userByEmail.getAccountRole()));
            return redirect(routes.Application.index());
        } else {
            flash("loginFailed", "Wrong username or password");
            return redirect(routes.LoginController.blank());
        }

    }

    public Result logout() {
        session().clear();
        flash("logout", "You've been logged out");
        return redirect(routes.LoginController.blank());
    }

}
