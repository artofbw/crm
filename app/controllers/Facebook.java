package controllers;

import models.User;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileManager;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.java.Secure;
import play.mvc.Controller;
import play.mvc.Result;
import services.UserDAO;

import java.util.List;
import java.util.Optional;


public class Facebook extends Controller {

    private List<CommonProfile> getProfiles() {
        final PlayWebContext context = new PlayWebContext(ctx());
        final ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
        return profileManager.getAll(true);
    }

    @Secure(clients = "FacebookClient")
    public Result facebookIndex() {

        PlayWebContext context = new PlayWebContext(ctx());
        ProfileManager<CommonProfile> profileManager = new ProfileManager(context);
        Optional<CommonProfile> profile = profileManager.get(true);

        String login = profile.get().getDisplayName();

        User userByName = UserDAO.findByName(login);

        if(userByName != null) {
            session("userId", String.valueOf(userByName.getId()));
            session("name", String.valueOf(userByName.getName()));
            session("userRole", String.valueOf(userByName.getAccountRole()));
        } else {
            User user = new User();

            user.setName(login);
            user.setEmail(profile.get().getEmail());
            user.setPassword(UserDAO.hashPassword(profile.get().getId()));
            user.save();

            session("userId", String.valueOf(userByName.getId()));
            session("name", String.valueOf(userByName.getName()));
            session("userRole", String.valueOf(userByName.getAccountRole()));
        }

        return redirect("/logoutfb");
    }
}
