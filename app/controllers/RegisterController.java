package controllers;

import com.google.inject.Inject;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.UserDAO;

public class RegisterController extends Controller {

    @Inject
    FormFactory formFactory;

    public Result blank() {
        if (session().isEmpty()) {
            Form<User> filledForm = formFactory.form(User.class);
            return ok(views.html.register.form.render(filledForm));
        } else {
            return redirect(routes.Application.index());
        }
    }

    public Result register() {

        Form<User> filledForm = formFactory.form(User.class).bindFromRequest();
        User newUser = filledForm.get();

        if (newUser != null && !newUser.getPassword().isEmpty() && !newUser.getName().isEmpty() && !newUser.getEmail().isEmpty()) {

            String name = newUser.getName();
            String email = newUser.getEmail();

            if (UserDAO.findByNameAndEmail(name, email) != null) {
                flash("userExists", "User or email already exsists.");
                return redirect(routes.RegisterController.blank());
            } else {

                String userPassword = newUser.getPassword();
                String hashedPassword = UserDAO.hashPassword(userPassword);
                newUser.setPassword(hashedPassword);
                newUser.save();

            }
            return redirect(routes.LoginController.blank());

        } else {
            return redirect(routes.Application.index());
        }
    }
}
