# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table contact (
  id                            bigserial not null,
  first_name                    varchar(255),
  last_name                     varchar(255),
  company                       varchar(255),
  email                         varchar(255),
  tel                           varchar(255),
  user_id                       bigint,
  constraint pk_contact primary key (id)
);

create table account (
  id                            bigserial not null,
  name                          varchar(255) not null,
  email                         varchar(255) not null,
  password                      varchar(255) not null,
  account_role                  varchar(5),
  picture                       varchar default 'default_avatar.png',
  constraint ck_account_account_role check (account_role in ('ADMIN','USER')),
  constraint uq_account_name unique (name),
  constraint uq_account_email unique (email),
  constraint pk_account primary key (id)
);

alter table contact add constraint fk_contact_user_id foreign key (user_id) references account (id) on delete restrict on update restrict;
create index ix_contact_user_id on contact (user_id);


# --- !Downs

alter table if exists contact drop constraint if exists fk_contact_user_id;
drop index if exists ix_contact_user_id;

drop table if exists contact cascade;

drop table if exists account cascade;

